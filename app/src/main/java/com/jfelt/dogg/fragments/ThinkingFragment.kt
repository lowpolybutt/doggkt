package com.jfelt.dogg.fragments

import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.textfield.TextInputEditText
import com.jfelt.dogg.R
import com.jfelt.dogg.Thinker
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.*

class ThinkingFragment : Fragment(), Thinker.OnDoggoDetectedListener, View.OnClickListener {

    private lateinit var thinking: TextView

    private val imgPath: String? by lazy { arguments?.getString("imgPath") }
    private val contentPath: String? by lazy { arguments?.getString("contentPath") }

    private lateinit var btnCorrect: Button
    private lateinit var btnIncorrect: Button

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_thinking, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        thinking = view.findViewById(R.id.thinking)

        btnIncorrect = view.findViewById(R.id.btn_incorrect)
        btnIncorrect.setOnClickListener(this)

        if (ThinkingFragmentArgs.fromBundle(arguments!!).imgPath != null) {
            thinkFromImgPath(ThinkingFragmentArgs.fromBundle(arguments!!).imgPath!!)
        } else if (ThinkingFragmentArgs.fromBundle(arguments!!).contentPath != null) {
            thinkFromContentPath(Uri.parse(ThinkingFragmentArgs.fromBundle(arguments!!).contentPath!!))
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_incorrect -> {
                val view = LayoutInflater.from(requireContext())
                    .inflate(R.layout.dialog_submission, null, false)
                val d = AlertDialog.Builder(requireActivity())
                    .setTitle("What was the answer?")
                    .setView(view)
                    .setPositiveButton("OK") {dialog, _ ->
                        val breed = view.findViewById<TextInputEditText>(R.id.breed_input).text?.toString()!!
                        checkSubmissionConsent(breed)
                        dialog.dismiss()
                    }
                    .setNegativeButton("Cancel") {dialog, _ ->
                        dialog.cancel()
                    }.create().show()
            }
        }
    }

    private fun checkSubmissionConsent(breed: String) {
        val a = ThinkingFragmentDirections.actionThinkingFragmentToPostThinkFragment(
            imgPath, contentPath, breed.toLowerCase(Locale.ENGLISH).replace(" ", "_")
        )
        findNavController().navigate(a)
    }

    private fun thinkFromImgPath(path: String) {
        val b = BitmapFactory.decodeFile(path)
        Thinker.think(b, this)
    }

    private fun thinkFromContentPath(u: Uri) {
//        val b = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//            ImageDecoder.decodeBitmap(
//                ImageDecoder.createSource(requireActivity().contentResolver, u)
//            )
//        } else {
//            MediaStore.Images.Media.getBitmap(requireActivity().contentResolver, u)
//        }
        val b = MediaStore.Images.Media.getBitmap(requireActivity().contentResolver, u)

        Thinker.think(b!!, this)
    }

    override fun onDoggoDetected(array: FloatArray?) {
        var p = Pair(0f, "")

        val labelReader = BufferedReader(InputStreamReader(requireActivity().assets.open("labels.txt")))

        for (i in array!!.indices) {
            val label = labelReader.readLine()
            if (array[i] > p.first) {
                p = Pair(array[i], label)
            }
        }

        thinking.text = "I am ${p.first * 100}% sure this is a ${p.second}"
    }
}