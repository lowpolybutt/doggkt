package com.jfelt.dogg.fragments

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Matrix
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.util.Size
import android.view.*
import android.widget.Button
import android.widget.Toast
import androidx.camera.core.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.fragment.findNavController
import com.jfelt.dogg.R
import java.io.File
import java.util.concurrent.Executors

private const val REQUEST_CODE_CAMERA = 10
private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)

class CameraFragment : Fragment(), LifecycleOwner {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_camera, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vf = view.findViewById(R.id.fragment_camera_viewfinder)

        if (allPermissionsGranted()) {
            vf.post { startCamera() }
        } else {
            requestPermissions(REQUIRED_PERMISSIONS, REQUEST_CODE_CAMERA)
        }

        vf.addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ -> updateTransform() }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_CODE_CAMERA) {
            if (allPermissionsGranted()) {
                vf.post { startCamera() }
            } else {
                Toast.makeText(requireContext(), "Required permissions denied", Toast.LENGTH_LONG).show()
            }
        }
    }

    private val executor = Executors.newSingleThreadExecutor()
    private lateinit var vf: TextureView

    private fun startCamera() {
        CameraX.unbindAll()
        val previewConf = PreviewConfig.Builder().apply {
            setTargetResolution(Size(640, 480))
        }.build()

        val pv = Preview(previewConf)

        pv.setOnPreviewOutputUpdateListener {
            val parent = vf.parent as ViewGroup
            parent.removeView(vf)
            parent.addView(vf, 0)

            vf.surfaceTexture = it.surfaceTexture
            updateTransform()
        }

        // This function shouldn't be called before view is initialised, can assume layout will not be null
        val v = view!!

        val imgCaptureConf = ImageCaptureConfig.Builder()

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            imgCaptureConf.setCaptureMode(ImageCapture.CaptureMode.MAX_QUALITY)
        }

        val imgCapture = ImageCapture(imgCaptureConf.build())
        v.findViewById<Button>(R.id.fragment_camera_take_picture).setOnClickListener {
            val f = File(requireActivity().externalMediaDirs.first(),
                "${System.currentTimeMillis()}.jpg")

            imgCapture.takePicture(f, executor, object : ImageCapture.OnImageSavedListener {
                override fun onImageSaved(file: File) {
                    Log.d("ML_DOGG", "File saved at ${file.absolutePath}")
                    val path = CameraFragmentDirections.actionCameraFragmentToDisplayProspectiveImageFragment(
                        file.absolutePath,
                        null
                    )

                    findNavController().navigate(path)
                }

                override fun onError(
                    imageCaptureError: ImageCapture.ImageCaptureError,
                    message: String,
                    cause: Throwable?
                ) {
                    Log.e("ML_DOGG", message, cause)
                    vf.post { Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show() }
                }
            })
        }

        CameraX.bindToLifecycle(this, pv, imgCapture)
    }

    private fun updateTransform() {
        val mtrx = Matrix()

        val cy = vf.width / 2f
        val cx = vf.height / 2f

        val rotDeg = when(vf.display.rotation) {
            Surface.ROTATION_0 -> 0
            Surface.ROTATION_90 -> 90
            Surface.ROTATION_180 -> 180
            Surface.ROTATION_270 -> 270
            else -> return
        }

        mtrx.postRotate(-rotDeg.toFloat(), cx, cy)

        vf.setTransform(mtrx)
    }

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all { ContextCompat
        .checkSelfPermission(requireContext(), it) == PackageManager.PERMISSION_GRANTED }
}