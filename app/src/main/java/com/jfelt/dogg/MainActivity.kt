package com.jfelt.dogg

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.navigation.findNavController
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.firebase.ml.common.modeldownload.FirebaseModelManager

class MainActivity : AppCompatActivity() {
    lateinit var modelManager: FirebaseModelManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val tb: BottomAppBar = findViewById(R.id.activity_main_app_bar)
        setSupportActionBar(tb)
        supportActionBar?.setHomeButtonEnabled(true)

        findNavController(R.id.activity_main_host_fragment).navigate(R.id.mainFragment)

        Thinker.downloadModel(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.settings -> {
                findNavController(R.id.activity_main_host_fragment)
                    .navigate(R.id.settingsFragment)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

//    override fun onBackPressed() {
//        Toast.makeText(this, "\"Back\" button disabled because it currently breaks everything",
//            Toast.LENGTH_LONG).show()
//    }
}